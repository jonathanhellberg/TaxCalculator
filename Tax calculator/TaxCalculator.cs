﻿using System;
using System.Collections.Generic;
using System.Linq;

//The focus should be on clean, simple and easy to read code 
//Everything but the public interface may be changed
namespace TaxCalculatorInterviewTests
{
    using CustomRateList = List<Tuple<DateTime, double>>;
    /// <summary>
    /// This is the public inteface used by our client and may not be changed
    /// </summary>
    public interface ITaxCalculator
    {
        double GetStandardTaxRate(Commodity commodity);
        void SetCustomTaxRate(Commodity commodity, double rate);
        double GetTaxRateForDateTime(Commodity commodity, DateTime date);
        double GetCurrentTaxRate(Commodity commodity);
    }

    /// <summary>
    /// Implements a tax calculator for our client.
    /// The calculator has a set of standard tax rates that are hard-coded in the class.
    /// It also allows our client to remotely set new, custom tax rates.
    /// Finally, it allows the fetching of tax rate information for a specific commodity and point in time.
    /// TODO: We know there are a few bugs in the code below, since the calculations look messed up every now and then.
    ///       There are also a number of things that have to be implemented.
    /// </summary>
    public class TaxCalculator : ITaxCalculator
    {
        private readonly static Dictionary<Commodity, CustomRateList> _customRates = [];
        private readonly Dictionary<Commodity, double> taxRates = new()
        {
            { Commodity.Alcohol, 0.25 },
            { Commodity.Food, 0.12 },
            { Commodity.FoodServices, 0.12 },
            { Commodity.Literature, 0.06 },
            { Commodity.Transport, 0.06 },
            { Commodity.CulturalServices, 0.06 }
        };


        /// <summary>
        /// Get the standard tax rate for a specific commodity.
        /// </summary>
        public double GetStandardTaxRate(Commodity commodity)
        {
            return taxRates.TryGetValue(commodity, out double rate) ? rate : 0.25;
        }


        /// <summary>
        /// This method allows the client to remotely set new custom tax rates.
        /// When they do, we save the commodity/rate information as well as the UTC timestamp of when it was done.
        /// NOTE: Each instance of this object supports a different set of custom rates, since we run one thread per customer.
        /// </summary>
        public void SetCustomTaxRate(Commodity commodity, double customRate)
        {
            //TODO: support saving multiple custom rates for different combinations of Commodity/DateTime
            //TODO: make sure we never save duplicates, in case of e.g. clock resets, DST etc - overwrite old values if this happens
            var newCustomTaxRate = Tuple.Create(DateTime.UtcNow, customRate);

            if (!_customRates.TryGetValue(commodity, out CustomRateList ? __))
            {
                _customRates[commodity] = [newCustomTaxRate];
                return;
            }

            var existingRateIndex = _customRates[commodity]
                .FindIndex(r => r.Item1.Date == newCustomTaxRate.Item1.Date);

            if (existingRateIndex != -1)
            {
                _customRates[commodity][existingRateIndex] = newCustomTaxRate;
                return;
            }
            
            _customRates[commodity].Add(newCustomTaxRate);
        }


        /// <summary>
        /// Gets the tax rate that is active for a specific point in time (in UTC).
        /// A custom tax rate is seen as the currently active rate for a period from its starting timestamp until a new custom rate is set.
        /// If there is no custom tax rate for the specified date, use the standard tax rate.
        /// </summary>
        public double GetTaxRateForDateTime(Commodity commodity, DateTime date)
        {
            if (!_customRates.TryGetValue(commodity, out CustomRateList ? __))
            {
                return this.GetStandardTaxRate(commodity);
            }

            var customRate = _customRates[commodity]
                .Where(kv => kv.Item1 <= date)
                .OrderByDescending(kv => kv.Item1)
                .FirstOrDefault();

            if (customRate == null)
            {
                return this.GetStandardTaxRate(commodity);
            }


            return customRate.Item2;
        }

        /// <summary>
        /// Gets the tax rate that is active for the current point in time.
        /// A custom tax rate is seen as the currently active rate for a period from its starting timestamp until a new custom rate is set.
        /// If there is no custom tax currently active, use the standard tax rate.
        /// </summary>
        public double GetCurrentTaxRate(Commodity commodity)
        {
            if (!_customRates.TryGetValue(commodity, out CustomRateList ? __))
            {
                return this.GetStandardTaxRate(commodity);
            }


            var latestCustomRate = _customRates[commodity]
                .OrderByDescending(customRate => customRate.Item1)
                .First();

            return latestCustomRate.Item2;
        }

    }

    public enum Commodity
    {
        //PLEASE NOTE: THESE ARE THE ACTUAL TAX RATES THAT SHOULD APPLY, WE JUST GOT THEM FROM THE CLIENT!
        Default,            //25%
        Alcohol,            //25%
        Food,               //12%
        FoodServices,       //12%
        Literature,         //6%
        Transport,          //6%
        CulturalServices    //6%
    }
}
