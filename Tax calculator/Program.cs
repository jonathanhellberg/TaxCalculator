﻿using TaxCalculatorInterviewTests;

internal class Program
{
    private static void Main()
    {
        var taxCalculator = new TaxCalculator();

        Console.WriteLine($"Standard tax rate for Alcohol: {taxCalculator.GetStandardTaxRate(Commodity.Alcohol) * 100:F2}%");

        taxCalculator.SetCustomTaxRate(Commodity.Alcohol, 0.10);
        Console.WriteLine($"Custom tax rate for Alcohol just set: {taxCalculator.GetCurrentTaxRate(Commodity.Alcohol) * 100:F2}%");

        var futureDate = DateTime.UtcNow.AddDays(1);
        Console.WriteLine($"Future tax rate for Alcohol at {futureDate}: {taxCalculator.GetTaxRateForDateTime(Commodity.Alcohol, futureDate) * 100:F2}%");

        taxCalculator.SetCustomTaxRate(Commodity.Alcohol, 0.20);
        taxCalculator.SetCustomTaxRate(Commodity.Alcohol, 0.15);
        Console.WriteLine($"Current tax rate for Alcohol after multiple updates: {taxCalculator.GetCurrentTaxRate(Commodity.Alcohol) * 100:F2}%");

        var pastDate = DateTime.UtcNow.AddDays(-30);
        Console.WriteLine($"Past tax rate for Alcohol at {pastDate}: {taxCalculator.GetTaxRateForDateTime(Commodity.Alcohol, pastDate) * 100:F2}%");

        var exactDateTime = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, DateTime.UtcNow.Hour, DateTime.UtcNow.Minute, 0);
        taxCalculator.SetCustomTaxRate(Commodity.Alcohol, 0.22);
        Console.WriteLine($"Tax rate for Alcohol set at exact boundary time {exactDateTime}: {taxCalculator.GetTaxRateForDateTime(Commodity.Alcohol, exactDateTime) * 100:F2}%");

        Console.WriteLine($"Tax rate for Alcohol before any custom rate was set: {taxCalculator.GetTaxRateForDateTime(Commodity.Alcohol, new DateTime(2020, 1, 1)) * 100:F2}%");

        taxCalculator.SetCustomTaxRate(Commodity.Food, 0.07);
        Console.WriteLine($"Custom tax rate for Food: {taxCalculator.GetCurrentTaxRate(Commodity.Food) * 100:F2}%");

        Console.WriteLine($"Standard rate for Literature (no custom rates set): {taxCalculator.GetStandardTaxRate(Commodity.Literature) * 100:F2}%");

        Console.WriteLine($"Requesting rate for an undefined commodity (default used): {taxCalculator.GetStandardTaxRate(Commodity.Default) * 100:F2}%");
    }
}